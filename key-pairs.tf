resource "aws_key_pair" "deployer" {
  key_name = "docker-swarm-example-deploy"
  #public_key = "${file(\"path-to-ssh-public-key\")}"
  # Also see variables.tf
  public_key = file("./docker_swarm_id_rsa.pub")
}