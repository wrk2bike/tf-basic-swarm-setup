#!/bin/bash
sudo apt-get -y install haproxy

echo '

# Mostly straight defaults

global
        log /dev/log    local0
        log /dev/log    local1 notice
        chroot /var/lib/haproxy
        stats socket /run/haproxy/admin.sock mode 660 level admin
        stats timeout 30s
        user haproxy
        group haproxy
        daemon

        # Default SSL material locations
        ca-base /etc/ssl/certs
        crt-base /etc/ssl/private

        # Default ciphers to use on SSL-enabled listening sockets.
        # For more information, see ciphers(1SSL). This list is from:
        #  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
        ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS
        ssl-default-bind-options no-sslv3

defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        #
        retries                 3
        timeout http-request    10s
        timeout queue           1m
        timeout connect         10s
        timeout client          1m
        timeout server          1m
        timeout http-keep-alive 10s
        timeout check           10s
        #
        errorfile 400 /etc/haproxy/errors/400.http
        errorfile 403 /etc/haproxy/errors/403.http
        errorfile 408 /etc/haproxy/errors/408.http
        errorfile 500 /etc/haproxy/errors/500.http
        errorfile 502 /etc/haproxy/errors/502.http
        errorfile 503 /etc/haproxy/errors/503.http
        errorfile 504 /etc/haproxy/errors/504.http

frontend thefront
        bind *:80
        stats uri /stats
        option forwardfor
        default_backend theswarm
        # Trying out this acl and use_backend statement:
        acl startswith_expirationperiodoverride_path     path -i -m beg /api/v1/catalogparts/ /commerce/api/v1/catalogparts/
        acl endswith_expirationperiodoverridepath        path -i -m end /expirationPeriodOverride /expirationPeriodOverride/
        use_backend expirationperiodoverride_force_503_response if startswith_expirationperiodoverride_path endswith_expirationperiodoverridepath

backend theswarm
        balance roundrobin
        # server swarm1 Your-Webserver1-IP:80 check
        # server swarm2 Your-Webserver2-IP:80 check
        #
        # Traefik needs the hostname to route so this may show as down even though its running
        server swarm1 my-ec2-swarm-manager:80 check
        # option httpchk HEAD / HTTP/1.0
        option httpchk GET / "HTTP/1.0\r\nHost: whoami.docker.foo"
        # option httpchk GET / "HTTP/1.0 Host: whoami.docker.foo"

backend expirationperiodoverride_force_503_response

' > /etc/haproxy/haproxy.cfg
