variable "access_key" {
	default = "YOUR_ACCESS_KEY"
}
variable "secret_key" {
	default = "YOUR_SECRET_KEY"
}
variable "region" {
  default = "your-region"
}
variable "docker_swarm_private_key_file" {
	default = "./docker_swarm_id_rsa"
}