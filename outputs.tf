output "haproxy-ip" {
  value = "${aws_instance.haproxy_load_balancer.0.public_ip}"
}
output "docker-manager-ip" {
  value = "${aws_instance.manager.public_ip}"
}
output "docker-workers" {
  value = "${aws_instance.worker.*.public_ip}"
}

output "prometheus-monitor" {
  value = "${aws_instance.prometheus_monitor.*.public_ip}"
}