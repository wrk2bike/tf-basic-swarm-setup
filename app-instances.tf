/* Setup our aws provider */
provider "aws" {
  # access_key  = "${var.access_key}"
  # secret_key  = "${var.secret_key}"
  #
  profile    = "default"
  region     = "us-east-1"
}

resource "aws_instance" "manager" {
  ami           = "ami-2757f631"
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.swarm.name}"]
  key_name = aws_key_pair.deployer.key_name
  connection {
    type     = "ssh"
    user     = "ubuntu"
    host     = self.public_dns
    private_key = file(var.docker_swarm_private_key_file)
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y curl apt-transport-https ca-certificates software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - ",
      "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable' | tee /etc/apt/sources.list.d/docker.list",
      "sudo apt-get update",
      "sudo apt-get install -y docker-ce",
      "sudo systemctl start docker.service",
      "sudo systemctl enable docker.service",
      "sudo curl -L \"https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",
      "sudo docker swarm init",
      "sudo docker swarm join-token --quiet worker > /home/ubuntu/token"
    ]
  }
  provisioner "file" {
    source = "nginx-proj"
    destination = "/home/ubuntu/"
  }
  provisioner "file" {
    source = "traefik-proj"
    destination = "/home/ubuntu/"
  }
  tags = {
    Name = "swarm-manager"
  }
}

resource "aws_instance" "worker" {
  count         = 2
  ami           = "ami-2757f631"
  instance_type = "t2.micro"
  security_groups = ["${aws_security_group.swarm.name}"]
  key_name = aws_key_pair.deployer.key_name
  connection {
    type     = "ssh"
    user     = "ubuntu"
    host     = self.public_dns
    private_key = file(var.docker_swarm_private_key_file)
  }
  provisioner "file" {
    source = var.docker_swarm_private_key_file
    destination = "/home/ubuntu/docker_swarm_id_rsa.pem"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y curl apt-transport-https ca-certificates software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - ",
      "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable' | tee /etc/apt/sources.list.d/docker.list",
      "sudo apt-get update",
      "sudo apt-get install -y docker-ce",
      "sudo systemctl start docker.service",
      "sudo systemctl enable docker.service",
      "sudo chmod 400 /home/ubuntu/docker_swarm_id_rsa.pem",
      "sudo scp -o StrictHostKeyChecking=no -o NoHostAuthenticationForLocalhost=yes -o UserKnownHostsFile=/dev/null -i docker_swarm_id_rsa.pem ubuntu@${aws_instance.manager.private_ip}:/home/ubuntu/token .",
      "sudo docker swarm join --token $(cat /home/ubuntu/token) ${aws_instance.manager.private_ip}:2377"
    ]
  }
  tags = {
    Name = "swarm-worker-${count.index}"
  }
}

resource "aws_instance" "haproxy_load_balancer" {
    security_groups = ["${aws_security_group.swarm.name}", "limited-http-https"]
    instance_type = "t2.micro"
    ami = "ami-2757f631"
    connection {
        user = "ubuntu"
        type     = "ssh"
        host = self.public_dns
        private_key = file(var.docker_swarm_private_key_file)
    }

    key_name = aws_key_pair.deployer.key_name
    count = 1

    provisioner "file" {
        source = "tf-remote-haproxy-script.sh"
        destination = "/tmp/tf-remote-haproxy-script.sh"
    }

    provisioner "remote-exec" {
      inline = [
        "sudo apt-get -y update",
        "sudo cp /etc/hosts /tmp/hosts",
        "sudo chmod 777 /tmp/hosts",
        "sudo echo ${aws_instance.manager.private_ip} my-ec2-swarm-manager >> /tmp/hosts",
        "sudo chmod 644 /tmp/hosts",
        "sudo cp /tmp/hosts /etc/hosts"
      ]
    }

    provisioner "remote-exec" {
        # some `sleep` might be needed to prevent i/o timeout here?
        # details: https://github.com/hashicorp/terraform/issues/1449#issuecomment-91600948
        inline = [
          #"sleep 420",
          "sleep 60",
          "chmod +x /tmp/tf-remote-haproxy-script.sh",
          "sudo /tmp/tf-remote-haproxy-script.sh",
          "sudo systemctl restart haproxy"
        ]
    }

    tags = {
      Name = "haproxy"
    }

    depends_on = [aws_instance.manager]
}

resource "aws_instance" "prometheus_monitor" {
    security_groups = ["${aws_security_group.swarm.name}", "limited-http-https"]
    instance_type = "t2.micro"
    ami = "ami-2757f631"
    connection {
        user = "ubuntu"
        type     = "ssh"
        host = self.public_dns
        private_key = file(var.docker_swarm_private_key_file)
    }

    key_name = aws_key_pair.deployer.key_name
    count = 1

    provisioner "remote-exec" {
      inline = [
        "sudo apt-get -y update",
        "sudo cp /etc/hosts /tmp/hosts",
        "sudo chmod 777 /tmp/hosts",
        "sudo echo ${aws_instance.manager.private_ip} my-ec2-swarm-manager >> /tmp/hosts",
        "sudo chmod 644 /tmp/hosts",
        "sudo cp /tmp/hosts /etc/hosts"
      ]
    }
    tags = {
      Name = "prometheus"
    }
    depends_on = [aws_instance.manager]
}
